package com.devcamp.s10.jbr4_30;

import java.util.ArrayList;
import java.util.Arrays;

public class Book {
    private String name ;
    private ArrayList<Author> authors ;
    private int qty = 0;
    private double price ;

    public Book(String name, ArrayList<Author> authors, int qty, double price) {
        this.name = name;
        this.authors = authors;
        this.qty = qty;
        this.price = price;
    }
    
    public Book(String name, ArrayList<Author> authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<Author> authors) {
        this.authors = authors;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book [authors=" + authors + "]"+ ", name=" + name + ", price=" + price + ", qty=" + qty + "]";
    }
    
    
    
}
