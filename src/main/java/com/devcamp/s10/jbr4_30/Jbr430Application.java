package com.devcamp.s10.jbr4_30;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr430Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr430Application.class, args);
	}

}
