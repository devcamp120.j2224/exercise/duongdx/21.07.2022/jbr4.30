package com.devcamp.s10.jbr4_30;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookController {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getListbook(){
        Author TacGia1 = new Author("To Huu", "tohuu@gmail.com", 'm');
        Author TacGia2 = new Author("Vu Trong Phung", "VuTrongphun@gmail.com", 'm');
        Author TacGia3 = new Author("ho Xuan Huong", "hoXuanHuong@gmail.com", 'f');
        Author TacGia4 = new Author("Nguyen Du", "nguyendu@gmail.com", 'm');
        Author TacGia5 = new Author("nam cao", "namcao@gmail.com", 'm');
        Author TacGia6 = new Author("ngo tat to", "ngotatto@gmail.com", 'm');

        System.out.println(TacGia1);
        System.out.println(TacGia2);
        System.out.println(TacGia3);
        System.out.println(TacGia4);
        System.out.println(TacGia5);
        System.out.println(TacGia6);

        ArrayList<Author> authorlist1 = new ArrayList<>();
        ArrayList<Author> authorlist2 = new ArrayList<>();
        ArrayList<Author> authorlist3 = new ArrayList<>();

        authorlist1.add(TacGia1);
        authorlist1.add(TacGia2);
        authorlist2.add(TacGia3);
        authorlist2.add(TacGia4);
        authorlist3.add(TacGia5);
        authorlist3.add(TacGia6);

        Book Sach1 = new Book("Chu Be Loat Choat", authorlist1 , 20000);
        Book sach2 = new Book("truyen kieu", authorlist2, 2, 120000);
        Book sach3 = new Book("tat den", authorlist3, 6, 50000);

        System.out.println("====Sach===");
        System.out.println(Sach1);
        System.out.println(sach2);
        System.out.println(sach3);

        ArrayList<Book> danhSachTacPham = new ArrayList<>();
        danhSachTacPham.add(Sach1);
        danhSachTacPham.add(sach2);
        danhSachTacPham.add(sach3);
        return danhSachTacPham ;
    }
}
